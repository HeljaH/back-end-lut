This project is using Node.js, Express and mongoDB. 
First I followed an old tutorial at https://www.youtube.com/watch?v=gnsO8-xJ8rs. Still, I used the newest versions of node, mongo and express. 
So my code is not exactly the same with the tutorial. 

How to create the database in terminal:

First open you mongoDB, then:
use mycustomers //creates the file mycustomers and changes there
db.createCollection('users') //creates a collection (MongoDB 'table') called users
db.users.insert(
[
	{ first_name:"Hanna",last_name:"Virta",email:"hannav@gmail.com" },
	{ first_name:"Hannu",last_name:"Lähde",email:"hannul@yahoo.com" },
	{ first_name:"Hansu",last_name:"Johto",email:"female@gmail.com" },
	{ first_name:"Harri",last_name:"Akku",email:"male@gmail.com" }
]); //adds data to users-collection