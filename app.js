//Eka file back-endissa, katotaan mitä tästä tulee
console.log("Hello World!");

//requirements

const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const { check, validationResult } = require('express-validator');
const mongojs = require('mongojs');
const db = mongojs('mycustomers', ['users']);
var ObjectId = mongojs.ObjectId;

const app = express();
const PORT = process.env.PORT || 3000;

//View Engine Middleware
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

//Body Parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

//Set static path
app.use(express.static(path.join(__dirname, 'public')));

app.use(express.json());


//Tämä pitää olla use-funktioiden jälkeen, että homma toimii.
app.get('/',function(req,res){
    db.users.find(function (err, docs) {
        console.log(docs);
        res.render('index', {
            title: 'Customers',
            users: docs,
            errors: []
        });
    });

});

app.post("/users/add", [
        //Checking for errors! Huomaa function paikka, jos siirrät sitä, koodi ei toimi
        check('first_name').not().isEmpty().withMessage('Etunimi on oltava'),
        check('last_name').not().isEmpty().withMessage( 'Sukunimi on oltava'),
    ],

    function(req, res){
        const errors = validationResult(req);
        if( !errors.isEmpty()){
            // Oh noes.
            console.log('Error catched');
            console.log('ERRORS', errors.errors);
            res.render('index', {
                title: 'Customers',
                users: users,
                errors: errors.errors
            });
            return res.status(422).json({errors: errors.array});
        }else{
            var newUser = {
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email
            }
            //Err ja result on mongoDB:n määrittelemiä
            db.users.insert(newUser, function(err, result){
                if(err){
                    console.log(err);
                }
                res.redirect('/');
            });
        }
});

app.delete('/users/delete/:id',function (req, res) {
    db.users.remove({_id: ObjectId(req.params.id)},function (err, result) {
        if(err){
            console.log(err);
        }
        res.redirect('/');
    });
})

//listen on a port, call-back function
app.listen(PORT, function(){
    console.log('Server started on ', PORT)
});
